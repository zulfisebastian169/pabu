package com.android.pabu.di.module


import android.app.Application
import android.util.Log
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.di.MyLifeCycleOwner
import com.android.pabu.util.Globals
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Created by Hamzah Tossaro on 10/18/2017.
 */

@Module(includes = arrayOf(ViewModelModule::class))
class AppModule {

    val mClientId = "testclient"
    //val mBaseUrl = "https://staggingapi.wowbid.live/v1/" //dev
    val mBaseUrl = "http://devsales.wowbid.live/api/v1/" //prod

    @Provides
    fun provideExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    @Provides
    @Singleton
    fun file(application: Application): File {
        val file = File(application.cacheDir, "OkHttpCache")
        file.mkdirs()
        return file
    }

    @Provides
    @Singleton
    fun cache(file: File): Cache {
        return Cache(file, 10 * 1024 * 1024)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder: GsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            if (BuildConfig.LOG) Log.d("--OkHttp--", message)
        })
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache, httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val requestInterceptor = Interceptor { chain ->
            val original = chain.request();
            val request = original.newBuilder()
                    .header("clientKey", mClientId)
                    .method(original.method(), original.body())
                    .build();

            chain.proceed(request)
        }
        return OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(requestInterceptor)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun providePicasso(app: Application, client: OkHttpClient): Picasso {
        return Picasso.Builder(app)
                .downloader(OkHttp3Downloader(client))
                .listener{_, _, e -> e.printStackTrace()}
                .build()
    }

    @Provides
    fun provideMyLifeCycleOwner(): MyLifeCycleOwner {
        return MyLifeCycleOwner()
    }

    @Provides
    fun globals(myLifeCycleOwner: MyLifeCycleOwner, settingViewModel: SettingViewModel, userViewModel: UserViewModel, picasso: Picasso): Globals {
        return Globals(myLifeCycleOwner, settingViewModel, userViewModel, picasso)
    }
}