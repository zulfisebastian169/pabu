package com.android.pabu.di.service

import com.android.pabu.model.extra.*
import com.android.pabu.model.entity.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

interface ApiService {
    @POST("register")
    fun register(@Body user: User): Call<User>

    @POST("user/login")
    fun login(@Body user: User): Call<User>

    @GET("customer")
    fun getCustomer(@Header("accessToken") accessToken: String, @Query("email") email: String): Call<Customer>

    @POST("customer/bonus")
    fun addBonus(@Header("accessToken") accessToken: String, @Query("email") email: String): Call<Bonus>

    @GET("product")
    fun getPackage(@Header("accessToken") accessToken: String): Call<Response>

    @GET("transaction")
    fun getTransaction(@Header("accessToken") accessToken: String,
                       @Query("showPage") showPage: String,
                       @Query("date") date: String,
                       @Query("bySales") sales: Boolean,
                       @Query("customerEmail") customerEmail: String?): Call<Response>

    @GET("transaction")
    fun getTransaction(@Header("accessToken") accessToken: String,
                       @Query("showPage") showPage: String,
                       @Query("date") date: String,
                       @Query("customerEmail") customerEmail: String?): Call<Response>

    @GET("event")
    fun getEvent(@Header("accessToken") accessToken: String, @Query("tag") tag: String): Call<Response>

    @GET("payment")
    fun getPayment(@Header("accessToken") accessToken: String): Call<Response>

    @POST("transaction/create")
    fun addTrans(@Header("accessToken") accessToken: String,
                 @Query("eventId") eventId: String,
                 @Query("productId") productId: String,
                 @Query("customerId") customerId: String,
                 @Query("salesId") salesId: String): Call<Transaction>

    @POST("transaction/paid")
    fun paidTrans(@Header("accessToken") accessToken: String,
                 @Query("orderNo") orderNo: String,
                 @Query("paymentId") paymentId: String,
                 @Query("refNo") refNo: String): Call<Transaction>

}