package com.android.pabu.di.module

import com.android.pabu.di.service.ApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Hamzah Tossaro on 10/18/2017.
 */

@Module
class ServiceModule {
    @Provides
    @Singleton
    fun provideUserService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}