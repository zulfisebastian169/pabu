package com.android.pabu.di.module

import com.android.pabu.di.service.ApiService
import com.android.pabu.model.local.dao.EventDao
import com.android.pabu.model.local.dao.SettingDao
import com.android.pabu.model.local.dao.TransactionDao
import com.android.pabu.model.local.dao.UserDao
import com.android.pabu.repository.SettingRepository
import com.android.pabu.repository.TransactionRepository
import com.android.pabu.repository.UserRepository

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import java.util.concurrent.Executor

/**
 * Created by Hamzah Tossaro on 10/18/2017.
 */

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideSettingRepository(executor: Executor, settingDao: SettingDao): SettingRepository {
        return SettingRepository(executor, settingDao)
    }

    @Provides
    @Singleton
    fun provideUserRepository(executor: Executor, userService: ApiService, eventDao: EventDao, userDao: UserDao): UserRepository {
        return UserRepository(executor, userService, eventDao, userDao)
    }

    @Provides
    @Singleton
    fun provideTransactionRepository(executor: Executor, apiService: ApiService, transactionDao: TransactionDao): TransactionRepository{
        return TransactionRepository(executor, apiService, transactionDao)
    }
}