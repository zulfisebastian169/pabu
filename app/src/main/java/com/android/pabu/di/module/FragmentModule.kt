package com.android.pabu.di.module

import com.android.pabu.view.fragment.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
        internal abstract fun contributeGalleryFragment(): GalleryFragment

    @ContributesAndroidInjector
        internal abstract fun contributeTestimonyFragment(): TestimonyFragment

    @ContributesAndroidInjector
        internal abstract fun contributeDonationFragment(): DonationFragment

    @ContributesAndroidInjector
        internal abstract fun contributeAgendaFragment(): AgendaFragment

    @ContributesAndroidInjector
        internal abstract fun contributeContentFragment(): ContentFragment

    @ContributesAndroidInjector
        internal abstract fun contributeDetailFragment(): DetailFragment
}