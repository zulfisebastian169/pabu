package com.android.pabu.di.module

import android.app.Application
import android.arch.persistence.room.Room
import com.android.pabu.model.local.AppDatabase
import com.android.pabu.model.local.dao.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    val mDatabaseName = "MyDatabase.db"

    @Provides
    @Singleton
    internal fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, mDatabaseName)
                .fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    internal fun provideSettingDao(database: AppDatabase): SettingDao {
        return database.settingDao()
    }

    @Provides
    @Singleton
    internal fun provideUserDao(database: AppDatabase): UserDao {
        return database.userDao()
    }

    @Provides
    @Singleton
    internal fun provideEventDao(database: AppDatabase): EventDao {
        return database.eventDao()
    }

    @Provides
    @Singleton
    internal fun provideTransactionDao(database: AppDatabase): TransactionDao {
        return database.transactionDao()
    }
}