package com.android.pabu

import android.app.Activity
import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp
import com.android.pabu.di.component.DaggerAppComponent
import com.android.pabu.util.Globals
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject lateinit var Globals: Globals

    companion object {
        var context: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/segoe_ui.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build())
        DaggerAppComponent.builder().application(this).build().inject(this)
        context = getApplicationContext()
    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }
}