package com.android.pabu.repository

import android.util.Log
import com.android.pabu.BuildConfig
import com.android.pabu.di.service.ApiService
import com.android.pabu.model.extra.*
import com.android.pabu.model.entity.*
import com.android.pabu.model.local.dao.TransactionDao
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Created by Hamzah Tossaro on 10/18/2018.
 */

class TransactionRepository @Inject
constructor(val executor: Executor,
            val userService: ApiService,
            val transactionDao: TransactionDao) {
    var TAG = TransactionRepository::class.java.simpleName

    interface ListenerLocal {
        fun onInserted(id: Long){}
        fun onInserted(ids: Array<Long>){}
        fun onSelected(_user: User?){}
        fun onSelected(_transaction: Transaction?){}
        fun onUpdated(){}
        fun onDeleted(){}
    }

    interface ListenerRemote {
        fun onStartRemote()
        fun onProcessRemote(code: Int, errorBody: ResponseBody?, transaction: Transaction?) {}
        fun onProcessRemote(response: Response?) {}
        fun onProcessRemoteTrans(transactions: MutableList<Transaction>?) {}
        fun onStopRemote(type: String)
    }

    //--Transaction
    fun saveTrans(_transaction: Transaction?){
        executor.execute({ transactionDao.insertTransaction(_transaction!!) })
    }
    fun getTransLocal(listenerLocal: ListenerLocal?) {
        executor.execute({
            val trans = transactionDao.getTransaction()
            listenerLocal?.onSelected(trans)
        })
    }
    fun addTrans(token: String,
                 eventId: String,
                 productId:String,
                 customerId: String,
                 salesId:String,
                 listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            userService.addTrans(token,eventId,productId,customerId,salesId).enqueue(object : Callback<Transaction> {
                override fun onResponse(call: Call<Transaction>, response: retrofit2.Response<Transaction>) {
                    val transResp = response.body()
                    listenerRemote?.onProcessRemote(response.code(), response.errorBody(), transResp)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Transaction>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }
    fun paidTrans(token: String,
                  orderNo: String,
                  paymentId: String,
                  refNo: String,
                 listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            userService.paidTrans(token,orderNo,paymentId,refNo).enqueue(object : Callback<Transaction> {
                override fun onResponse(call: Call<Transaction>, response: retrofit2.Response<Transaction>) {
                    val transResp = response.body()
                    listenerRemote?.onProcessRemote(response.code(), response.errorBody(), transResp)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Transaction>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    fun getTransRemote(token: String, listenerRemote: ListenerRemote?, show: String, date: String, email: String?, sales: Boolean) {
        executor.execute({
            listenerRemote?.onStartRemote()
            userService.getTransaction(token, show, date, sales, email).enqueue(object : Callback<Response> {
                override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                    val custResp = response.body()
                    listenerRemote?.onProcessRemote(custResp)
                    listenerRemote?.onProcessRemoteTrans(custResp?.transactions)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Response>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    fun getTransRemote(token: String, listenerRemote: ListenerRemote?, show: String, date: String, email: String?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            userService.getTransaction(token, show, date, email).enqueue(object : Callback<Response> {
                override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                    val custResp = response.body()
                    listenerRemote?.onProcessRemote(custResp)
                    listenerRemote?.onProcessRemoteTrans(custResp?.transactions)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Response>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    fun truncateAll(){
        executor.execute({
            transactionDao.truncateTransaction()
        })
    }
}
