package com.android.pabu.repository

import android.arch.lifecycle.MutableLiveData
import com.android.pabu.model.entity.Setting
import com.android.pabu.model.local.dao.SettingDao
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Created by Hamzah Tossaro on 10/25/2018.
 */

class SettingRepository @Inject
constructor(val executor: Executor, val settingDao: SettingDao) {

    var TAG = SettingRepository::class.java.simpleName

    interface listener {
        fun onStartRemote()
        fun onProcessRemote()
        fun onStopRemote()
    }

    interface ListenerLocal {
        fun onInserted(id: Long){}
        fun onInserted(ids: Array<Long>){}
        fun onSelected(_setting: Setting?){}
        fun onUpdated(){}
        fun onDeleted(){}
    }

    //-- Setting
    var mSetting: MutableLiveData<Setting> = MutableLiveData()

    fun saveSetting(setting: Setting){
        executor.execute({ settingDao.insertSetting(setting) })
    }
    fun getSettingLocal(uid: String, listenerLocal: ListenerLocal?) {
        executor.execute({
            val setting = settingDao.getSetting(uid)
            listenerLocal?.onSelected(setting)
        })
    }
    fun deleteSetting(setting: Setting){
        executor.execute({ settingDao.deleteSetting(setting) })
    }
    fun truncate(){
        executor.execute({ settingDao.truncate() })
    }
}
