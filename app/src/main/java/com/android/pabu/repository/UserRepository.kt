package com.android.pabu.repository

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.android.pabu.BuildConfig
import com.android.pabu.di.service.ApiService
import com.android.pabu.model.extra.*
import com.android.pabu.model.entity.*
import com.android.pabu.model.local.dao.EventDao
import com.android.pabu.model.local.dao.UserDao
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Created by Hamzah Tossaro on 10/18/2018.
 */

class UserRepository @Inject
constructor(val executor: Executor,
            val userService: ApiService,
            val eventDao: EventDao,
            val userDao: UserDao) {
    var TAG = UserRepository::class.java.simpleName

    interface ListenerLocal {
        fun onInserted(id: Long){}
        fun onInserted(ids: Array<Long>){}
        fun onSelected(_user: User?){}
        fun onSelected(_event: Event?){}
        fun onUpdated(){}
        fun onDeleted(){}
    }

    interface ListenerRemote {
        fun onStartRemote()
        fun onProcessRemote(user: User?) {}
        fun onProcessRemote(bonus: Bonus?) {}
        fun onProcessRemote(code: Int, errorBody: ResponseBody?, user: User?) {}
        fun onProcessRemote(code: Int, errorBody: ResponseBody?, transaction: Transaction?) {}
        fun onProcessRemote(code: Int, errorBody: ResponseBody?, bonus: Bonus?) {}
        fun onProcessRemote(profile: Profile?) {}
        fun onProcessRemote(customer: Customer?) {}
        fun onProcessRemote(response: Response?) {}
        fun onProcessRemote(event: Event?) {}
        fun onProcessRemotePA(products: MutableList<Product>?) {}
        fun onProcessRemoteEvent(events: MutableList<Event>?) {}
        fun onProcessRemotePayment(payments: MutableList<Payment>?) {}
        fun onProcessRemoteTrans(transactions: MutableList<Transaction>?) {}
        fun onStopRemote(type: String)
    }

    //--User
    var hRunUser: Runnable? = null
    var mUser: MutableLiveData<User> = MutableLiveData()
    fun saveUser(user: User){
        executor.execute({ userDao.insertUser(user) })
    }
    fun updateUserLocal(user: User){
        executor.execute({ userDao.updateUserLocal(user) })
    }
    fun getUserLocal(listenerLocal: ListenerLocal?) {
        executor.execute({
            val user = userDao.getUser()
            listenerLocal?.onSelected(user)
        })
    }
    fun getUserRemote(user: User, listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            userService.login(user).enqueue(object : Callback<User> {
                override fun onResponse(call: Call<User>, response: retrofit2.Response<User>) {
                    val userResp = response.body()
                    if (userResp != null) mUser.postValue(userResp)
                    listenerRemote?.onProcessRemote(response.code(), response.errorBody(), userResp)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<User>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    //--Event
    fun saveEvent(event: Event){
        executor.execute({ eventDao.insertEvent(event) })
    }
    fun getEventLocal(listenerLocal: ListenerLocal?) {
        executor.execute({
            val event = eventDao.getEvent()
            listenerLocal?.onSelected(event)
        })
    }
    fun getEventRemote(tag: String, listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            val user = userDao.getUser()
            if(user!=null) userService.getEvent(user.token!!, tag).enqueue(object : Callback<Response> {
                override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                    val eventResp = response.body()
                    listenerRemote?.onProcessRemoteEvent(eventResp?.events)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Response>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    //Payment
    fun getPaymentRemote(listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            val user = userDao.getUser()
            if(user!=null) userService.getPayment(user.token!!).enqueue(object : Callback<Response> {
                override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                    val paymentResp = response.body()
                    listenerRemote?.onProcessRemotePayment(paymentResp?.payments)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Response>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    //--Bonus
    fun addBonus(email: String, listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            val user = userDao.getUser()
            if(user!=null) userService.addBonus(user.token!!, email).enqueue(object : Callback<Bonus> {
                override fun onResponse(call: Call<Bonus>, response: retrofit2.Response<Bonus>) {
                    val bonusResp = response.body()
                    listenerRemote?.onProcessRemote(response.code(), response.errorBody(), bonusResp)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Bonus>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    //--Customer
    fun getCustomerRemote(email: String, listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            val user = userDao.getUser()
            if(user!=null) userService.getCustomer(user.token!!, email).enqueue(object : Callback<Customer> {
                override fun onResponse(call: Call<Customer>, response: retrofit2.Response<Customer>) {
                    val custResp = response.body()
                    if(custResp!=null) listenerRemote?.onProcessRemote(custResp)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Customer>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    //--Product
    fun getProductAppsRemote(listenerRemote: ListenerRemote?) {
        executor.execute({
            listenerRemote?.onStartRemote()
            val user = userDao.getUser()
            if(user!=null) userService.getPackage(user.token!!).enqueue(object : Callback<Response> {
                override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                    val coinResp = response.body()
                    listenerRemote?.onProcessRemotePA(coinResp?.products)
                    listenerRemote?.onStopRemote("response")
                }
                override fun onFailure(call: Call<Response>, t: Throwable) {
                    listenerRemote?.onStopRemote("failure")
                    if (BuildConfig.LOG) Log.e("---error---",t.cause.toString())
                }
            })
        })
    }

    fun truncateAll(){
        executor.execute({
            userDao.truncateUser()
        })
    }
}
