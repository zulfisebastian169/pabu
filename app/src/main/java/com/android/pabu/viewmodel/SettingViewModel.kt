package com.android.pabu.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.android.pabu.model.entity.Setting

import com.android.pabu.repository.SettingRepository

import javax.inject.Inject


/**
 * Created by Hamzah Tossaro on 10/25/2018.
 */

class SettingViewModel @Inject
constructor(val settingRepo: SettingRepository) : ViewModel() {
    var mSetting: LiveData<Setting>? = settingRepo.mSetting

    //-- Setting
    fun saveSetting(setting: Setting){
        settingRepo.saveSetting(setting)
    }
    fun getSettingLocal(uid:String, listenerLocal: SettingRepository.ListenerLocal?) {
        settingRepo.getSettingLocal(uid, listenerLocal)
    }
    fun deleteSetting(setting: Setting){
        settingRepo.deleteSetting(setting)
    }
    fun truncate(){
        settingRepo.truncate()
    }

    override fun onCleared() {
        super.onCleared()
    }
}