package com.android.pabu.viewmodel

import android.arch.lifecycle.ViewModel
import com.android.pabu.model.entity.*
import com.android.pabu.repository.TransactionRepository

import javax.inject.Inject


/**
 * Created by Hamzah Tossaro on 10/18/2018.
 */

class TransactionViewModel @Inject
constructor(val transactionRepo: TransactionRepository) : ViewModel() {
    //Trans
    fun saveTrans(transaction: Transaction){
        transactionRepo.saveTrans(transaction)
    }
    fun getTransLocal(listenerLocal: TransactionRepository.ListenerLocal?) {
        transactionRepo.getTransLocal(listenerLocal)
    }
    fun addTrans(token: String,
                 eventId: String,
                 productId:String,
                 customerId: String,
                 salesId:String,
                 listenerRemote: TransactionRepository.ListenerRemote?) {
        transactionRepo.addTrans(token, eventId,productId,customerId,salesId,listenerRemote)
    }
    fun paidTrans(token: String,
                 orderNo: String,
                 paymentId: String,
                 refNo: String,
                 listenerRemote: TransactionRepository.ListenerRemote?) {
        transactionRepo.paidTrans(token,orderNo,paymentId,refNo,listenerRemote)
    }
    fun getTransRemote(token: String, show: String, date: String, email: String?, sales: Boolean, listenerRemote: TransactionRepository.ListenerRemote?) {
        transactionRepo.getTransRemote(token, listenerRemote, show, date, email, sales)
    }
    fun getTransRemote(token: String, show: String, date: String, email: String?, listenerRemote: TransactionRepository.ListenerRemote?) {
        transactionRepo.getTransRemote(token, listenerRemote, show, date, email)
    }

    fun truncateAll(){
        transactionRepo.truncateAll()
    }

    override fun onCleared() {
        super.onCleared()
    }
}