package com.android.pabu.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.android.pabu.model.entity.*

import com.android.pabu.repository.UserRepository

import javax.inject.Inject


/**
 * Created by Hamzah Tossaro on 10/18/2018.
 */

class UserViewModel @Inject
constructor(val userRepo: UserRepository) : ViewModel() {
    //--User
    var mUser: LiveData<User>? = userRepo.mUser
    fun saveUser(user: User){
        userRepo.saveUser(user)
    }
    fun updateUser(user: User){
        userRepo.updateUserLocal(user)
    }
    fun getUserLocal(listenerLocal: UserRepository.ListenerLocal?) {
        userRepo.getUserLocal(listenerLocal)
    }
    fun getUserRemote(user: User, listenerRemote: UserRepository.ListenerRemote) {
        userRepo.getUserRemote(user, listenerRemote)
    }

    //Bonus
    fun addBonus(email:String, listenerRemote: UserRepository.ListenerRemote?) {
        userRepo.addBonus(email, listenerRemote)
    }

    //Customer
    fun getCustomerRemote(email:String, listenerRemote: UserRepository.ListenerRemote?) {
        userRepo.getCustomerRemote(email, listenerRemote)
    }

    //Event
    fun saveEvent(event: Event){
        userRepo.saveEvent(event)
    }
    fun getEventLocal(listenerLocal: UserRepository.ListenerLocal?) {
        userRepo.getEventLocal(listenerLocal)
    }
    fun getEventRemote(tag:String, listenerRemote: UserRepository.ListenerRemote?) {
        userRepo.getEventRemote(tag, listenerRemote)
    }

    //Payment
    fun getPaymentRemote(listenerRemote: UserRepository.ListenerRemote?) {
        userRepo.getPaymentRemote(listenerRemote)
    }

    //--Product
    fun getProductAppsRemote(listenerRemote: UserRepository.ListenerRemote?) {
        userRepo.getProductAppsRemote(listenerRemote)
    }

    fun truncateAll(){
        userRepo.truncateAll()
    }

    override fun onCleared() {
        super.onCleared()
    }
}