package com.android.pabu.view.activity

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBar
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import co.zsmb.materialdrawerkt.builders.DrawerBuilderKt
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.badgeable.secondaryItem
import co.zsmb.materialdrawerkt.draweritems.divider
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import com.android.pabu.R
import com.android.pabu.util.Constants
import com.joanzapata.iconify.Iconify
import com.joanzapata.iconify.fonts.IoniconsModule
import com.android.pabu.util.Globals
import com.android.pabu.view.fragment.*
import com.android.pabu.viewmodel.UserViewModel
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import com.mikepenz.materialdrawer.Drawer
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_home.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject



class HomeActivity : AppCompatActivity(), HasSupportFragmentInjector {
    var TAG: String = HomeFragment::class.java.simpleName
    var mContext: Context = this
    var mBackPressed: Long = 0
    var userViewModel: UserViewModel? = null
    var drawers: Drawer? = null
    var context: Context? = null

    @Inject lateinit var Globals: Globals
    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        Iconify.with(IoniconsModule())
        supportPostponeEnterTransition()
        context = baseContext


        drawers = drawer {
            sliderBackgroundColorRes = R.color.blue
//            accountHeader {
//                background = R.color.primary
//                profile("Samantha", "samantha@gmail.com") {
//                    iconUrl = "http://some.site/samantha.png"
//                }
//                profile("Laura", "laura@gmail.com") {
//                    icon = R.drawable.ico_avatar
//                }
//            }
            gravity = Gravity.END
            primaryItem(R.string.drawer_home) {
                icon = R.drawable.ic_drawer_home
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, HomeFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "BERANDA"
                    false
                }
            }
            primaryItem(R.string.drawer_profile) {
                icon = R.drawable.ic_drawer_profile
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, ProfileFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "PROFILE"
                    false
                }
            }
            primaryItem(R.string.drawer_agenda) {
                icon = R.drawable.ic_drawer_agenda
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, AgendaFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "Agenda"
                    false
                }
            }
            primaryItem(R.string.drawer_programs) {
                icon = R.drawable.ic_drawer_program
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, ContentFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "Program"
                    false
                }
            }
            primaryItem(R.string.drawer_gallery) {
                icon = R.drawable.ic_drawer_gallery
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, GalleryFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "Galeri"
                    false
                }
            }
            primaryItem(R.string.drawer_testi) {
                icon = R.drawable.ic_drawer_testimony
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, TestimonyFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "Kata Mereka"
                    false
                }
            }
            primaryItem(R.string.drawer_kalkulator) {
                icon = R.drawable.ic_drawer_calculator
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.blue
                textColorRes = R.color.white
                iconTintingEnabled = true
            }
            primaryItem(R.string.drawer_rekening) {
                icon = R.drawable.ic_drawer_rekening
                iconColorRes = R.color.white
                selectedIconColorRes = R.color.white
                textColorRes = R.color.white
                selectedTextColorRes = R.color.white
                selectedColorRes = R.color.red
                iconTintingEnabled = true
                onClick { _ ->
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.root_container, DonationFragment.create())
                            .addToBackStack(TAG)
                            .commit()
                    tv_title.text = "Donasi"
                    false
                }
            }
        }

        bar_menu.setOnClickListener {
            drawers?.openDrawer()
        }

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.root_container, HomeFragment.create()).commit()
        }

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawers?.openDrawer()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onResume() {
        super.onResume()
    }

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment>? {
        return dispatchingAndroidInjector
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        return if (Constants.isLoading==false) super.dispatchTouchEvent(ev) else true
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) supportFragmentManager.popBackStack()
        else {
            if (mBackPressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed()
                return
            } else Globals.myToast(this, "Tap again to close apps")
            mBackPressed = System.currentTimeMillis()
        }
    }
}
