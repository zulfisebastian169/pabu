package com.android.pabu.view.fragment

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.view.*
import com.android.pabu.R
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.android.pabu.model.entity.Brand
import com.android.pabu.model.extra.Banner
import com.android.pabu.model.extra.Testimony
import com.android.pabu.util.*
import com.android.pabu.view.adapter.pager.SlideBannerPagerAdapter
import com.android.pabu.view.adapter.recycler.BrandRecyclerAdapter
import com.android.pabu.view.adapter.recycler.TestiRecyclerAdapter
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_home.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import org.bouncycastle.util.test.Test
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.CameraPosition
import android.text.method.TextKeyListener.clear
import com.google.android.gms.maps.*


class ProfileFragment: BaseFragment() {

    var TAG: String = ProfileFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null
    var accessToken: String? = null
    var phone: String? = null
    var mMap: GoogleMap? = null


    var lProduct: MutableList<String> = mutableListOf<String>()

    private lateinit var mDrawerLayout: DrawerLayout

    companion object {
        fun create(): ProfileFragment = ProfileFragment()
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_profile, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun initialize() {
        var layoutManager = CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true);
        layoutManager.setPostLayoutListener(CarouselZoomPostLayoutListener())

        if (mMap == null) {
            var mapFragment = childFragmentManager.findFragmentById(R.id.fragment_map) as WorkaroundMapFragment
            mapFragment!!.getMapAsync(object : OnMapReadyCallback{
                override fun onMapReady(googleMap: GoogleMap?) {
                    mMap = googleMap;
                    mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                    mMap?.getUiSettings()?.isZoomControlsEnabled = true
                    mMap?.getUiSettings()?.setMapToolbarEnabled(true)
                    mMap?.getUiSettings()?.isMyLocationButtonEnabled = true
                    mMap?.getUiSettings()?.isScrollGesturesEnabled = false
                    val googlePlex = CameraPosition.builder()
                            .target(LatLng(-6.279204,106.95139))
                            .zoom(18f)
                            .bearing(0f)
                            .tilt(45f)
                            .build()

                    mMap?.addMarker(MarkerOptions().position(LatLng(-6.279204,106.95139)).title("Yayasan Pundi Amal Bakti Ummat"));
                    mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(googlePlex))

                    (childFragmentManager.findFragmentById(R.id.fragment_map) as WorkaroundMapFragment).setListener( object : WorkaroundMapFragment.OnTouchListener{
                        override fun onTouch() {
                            scrollViewer.requestDisallowInterceptTouchEvent(false)
                        }
                    })
                }
            })
        }

        telp_1.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(mContext!!, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), Constants.CALL_PHONE_PERMISSION)
            }else{
                phone = (mContext!!).getString(R.string.home_telp)
                callIt()
            }
        }

        hp_1.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(mContext!!, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), Constants.CALL_PHONE_PERMISSION)
            }else{
                phone = (mContext!!).getString(R.string.home_hp_1)
                callIt()
            }
        }

        hp_2.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(mContext!!, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), Constants.CALL_PHONE_PERMISSION)
            }else{
                phone = (mContext!!).getString(R.string.home_hp_2)
                callIt()
            }
        }


    }

    fun callIt(){
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phone))
        startActivity(intent)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.CALL_PHONE_PERMISSION -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                    callIt()
                }
            }
        }
    }
}