package com.android.pabu.view.adapter.recycler

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.android.pabu.R
import com.android.pabu.model.entity.Brand
import com.google.firebase.crash.FirebaseCrash
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.util.Globals

class BrandRecyclerAdapter(
        val layout_id: Int,
        val brands: MutableList<Brand>,
        val mListener: BrandClickListener?,
        val Globals: Globals) : RecyclerView.Adapter<BrandRecyclerAdapter.BrandHolder>() {
    var context: Context? = null
    var selected = -1

    interface BrandClickListener {
        fun onBrandClicked(holder: BrandHolder, brand: Brand)
    }

    inner class BrandHolder(_itemView: View) : RecyclerView.ViewHolder(_itemView) {
        var container: CardView? = null
        var image: ImageView? = null
        var name: TextView? = null

        init {
            container = _itemView.findViewById<View>(R.id.container) as CardView
            if (container != null) container!!.preventCornerOverlap = false

            image   = _itemView.findViewById(R.id.product_image)
        }
    }

    override fun getItemCount(): Int {
        return this.brands.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandHolder {
        context = parent.context
        val layoutView = LayoutInflater.from(context).inflate(layout_id, null)
        val rcv = BrandHolder(layoutView)

        return rcv
    }

    override fun onBindViewHolder(holder: BrandHolder, position: Int) {
        try {
            val brand = brands[position]

            holder.container!!.setOnClickListener {
                selected = holder.adapterPosition
                mListener?.onBrandClicked(holder, brand)
            }

            holder.name?.text = Globals.fromHtml(brand.name!!)

            Globals.picasso.load(brand.photoUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.image, object : com.squareup.picasso.Callback {
                    override fun onSuccess() {}
                    override fun onError() {
                        Globals.picasso.load(brand.photoUrl).into(holder.image)
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
            FirebaseCrash.report(e)
            if (BuildConfig.LOG) Log.d("-----error-----", e.message)
        }

    }
}