package com.android.pabu.view.adapter.recycler

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import com.android.pabu.R
import com.google.firebase.crash.FirebaseCrash
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.model.extra.Content
import com.android.pabu.util.Globals
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation


class ContentRecyclerAdapter(
        val layout_id: Int,
        val Contents: MutableList<Content>,
        val mListener: ContentClickListener?,
        val Globals: Globals) : RecyclerView.Adapter<ContentRecyclerAdapter.ContentHolder>() {
    var context: Context? = null
    var selected = -1

    interface ContentClickListener {
        fun onContentClicked(holder: ContentHolder, agenda: Content, position: Int)
    }

    inner class ContentHolder(_itemView: View) : RecyclerView.ViewHolder(_itemView) {
        var container: CardView? = null
        var image: ImageView? = null
        var name: TextView? = null

        init {
            container = _itemView.findViewById<View>(R.id.container) as CardView
            if (container != null) container!!.preventCornerOverlap = false

            image   = _itemView.findViewById(R.id.content_image)
            name   = _itemView.findViewById(R.id.content_title)
        }
    }

    override fun getItemCount(): Int {
        return this.Contents.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentHolder {
        context = parent.context
        val rootView = LayoutInflater.from(context).inflate(layout_id, null, false)
        val lp = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        rootView.setLayoutParams(lp)
        return ContentHolder(rootView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ContentHolder, position: Int) {
        try {
            val Content = Contents[position]

            holder.container!!.setOnClickListener {
                selected = holder.adapterPosition
                mListener?.onContentClicked(holder, Content, position)
            }

            holder.image?.transitionName = "imageContent_" + position
            holder.name?.transitionName = "agendaName_" + position
//
//            holder.name?.text = Globals.fromHtml(Content.name!!)
//
            Globals.picasso.load(Content.photoUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit().centerCrop()
                .transform(RoundedCornersTransformation(20,0))
                .into(holder.image, object : com.squareup.picasso.Callback {
                    override fun onSuccess() {}
                    override fun onError() {
                        Globals.picasso.load(Content.photoUrl)
                                .fit().centerCrop()
                                .transform(RoundedCornersTransformation(20 ,0))
                                .into(holder.image)
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
            FirebaseCrash.report(e)
            if (BuildConfig.LOG) Log.d("-----error-----", e.message)
        }

    }
}