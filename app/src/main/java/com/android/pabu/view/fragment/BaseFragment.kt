package com.android.pabu.view.fragment

import android.arch.lifecycle.ViewModelProvider
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.android.pabu.util.Globals
import java.util.concurrent.Executor
import javax.inject.Inject

abstract class BaseFragment: Fragment() {

    @Inject lateinit var Globals: Globals
    @Inject lateinit var executor: Executor
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View? = myCreateView(inflater,container,savedInstanceState);
        return rootView
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        var animation = super.onCreateAnimation(transit, enter, nextAnim)

        if (Build.VERSION.SDK_INT != 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim)
            }

            if (animation != null) {
                if(getView()?.isHardwareAccelerated()!!) getView()?.setLayerType(View.LAYER_TYPE_HARDWARE, null)

                animation.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {}
                    override fun onAnimationRepeat(animation: Animation) {}
                    override fun onAnimationEnd(animation: Animation) {
                        getView()?.setLayerType(View.LAYER_TYPE_NONE, null)
                    }
                })
            }
        }
        return animation
    }
}