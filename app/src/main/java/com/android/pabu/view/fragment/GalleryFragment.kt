package com.android.pabu.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.view.*
import com.android.pabu.R
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel
import dagger.android.support.AndroidSupportInjection
import com.android.pabu.model.extra.Gallery
import com.android.pabu.view.adapter.recycler.GalleryRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_gallery.*


class GalleryFragment: BaseFragment() {

    var TAG: String = GalleryFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null
    var galleries: MutableList<Gallery> = arrayListOf()


    var lProduct: MutableList<String> = mutableListOf<String>()

    private lateinit var mDrawerLayout: DrawerLayout

    companion object {
        fun create(): GalleryFragment = GalleryFragment()
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_gallery, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun initialize() {
        var xGalleries = getResources().getStringArray(R.array.gallery)

        if(galleries.size<1){
            for (i in 0..5) {
                galleries.add(Gallery((i + 1), "Gambar Foto Tersenyum " + (i + 1), null, null, xGalleries[i].toString()))
            }
        }

        gallery_list?.setLayoutManager(LinearLayoutManager(mContext))
        gallery_list?.adapter = GalleryRecyclerAdapter(R.layout.row_gallery_list, galleries, object : GalleryRecyclerAdapter.GalleryClickListener{
            override fun onGalleryClicked(holder: GalleryRecyclerAdapter.GalleryHolder, Gallery: Gallery) {

            }
        },  Globals)

        var manager = GridLayoutManager(mContext,3)
        gallery_view?.setLayoutManager(manager)
        gallery_view?.adapter = GalleryRecyclerAdapter(R.layout.row_gallery_grid, galleries, object : GalleryRecyclerAdapter.GalleryClickListener{
            override fun onGalleryClicked(holder: GalleryRecyclerAdapter.GalleryHolder, Gallery: Gallery) {

            }
        }, Globals)

        changeLayout(3)

        change_layout.setOnClickListener {
            if(change_layout.tag.toString() == "list"){
                changeLayout(0)
                change_layout.tag = "grid"
                change_img.setImageResource(R.drawable.ic_grid)
            }else{
                changeLayout(3)
                change_layout.tag = "list"
                change_img.setImageResource(R.drawable.ic_list)
            }
        }
    }

    fun changeLayout(x: Int){
        if(x==0){
            gallery_list.visibility = View.VISIBLE
            gallery_view.visibility = View.GONE
        }else{
            gallery_view.visibility = View.VISIBLE
            gallery_list.visibility = View.GONE
        }
    }
}