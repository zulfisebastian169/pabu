package com.android.pabu.view.adapter.recycler

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.android.pabu.R
import com.google.firebase.crash.FirebaseCrash
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.model.extra.Testimony
import com.android.pabu.util.CircleTransform
import com.android.pabu.util.Globals
import jp.wasabeef.picasso.transformations.MaskTransformation

class TestiRecyclerAdapter(
        val layout_id: Int,
        val Testis: MutableList<Testimony>,
        val mListener: TestiClickListener?,
        val Globals: Globals) : RecyclerView.Adapter<TestiRecyclerAdapter.TestiHolder>() {
    var context: Context? = null
    var selected = -1

    interface TestiClickListener {
        fun onTestiClicked(holder: TestiHolder, Testi: Testimony)
    }

    inner class TestiHolder(_itemView: View) : RecyclerView.ViewHolder(_itemView) {
        var container: CardView? = null
        var image: ImageView? = null
        var name: TextView? = null
        var note1: TextView? = null
        var desc: TextView? = null

        init {
            container = _itemView.findViewById<View>(R.id.container) as CardView
            if (container != null) container!!.preventCornerOverlap = false

            image   = _itemView.findViewById(R.id.testi_image)
            name   = _itemView.findViewById(R.id.testi_name)
            note1   = _itemView.findViewById(R.id.testi_note1)
            desc   = _itemView.findViewById(R.id.testi_desc)
        }
    }

    override fun getItemCount(): Int {
        return this.Testis.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestiHolder {
        context = parent.context
        val layoutView = LayoutInflater.from(context).inflate(layout_id, null)
        val rcv = TestiHolder(layoutView)

        return rcv
    }

    override fun onBindViewHolder(holder: TestiHolder, position: Int) {
        try {
            val Testi = Testis[position]

            holder.container!!.setOnClickListener {
                selected = holder.adapterPosition
                mListener?.onTestiClicked(holder, Testi)
            }

            holder.name?.text = Globals.fromHtml(Testi.name!!)
            holder.desc?.text = Globals.fromHtml(Testi.description!!)
            holder.note1?.text = Globals.fromHtml(Testi.note_1!!)

            Globals.picasso.load(Testi.photoUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .transform(CircleTransform())
                .into(holder.image, object : com.squareup.picasso.Callback {
                    override fun onSuccess() {}
                    override fun onError() {
                        Globals.picasso.load(Testi.photoUrl)
                        .transform(CircleTransform())
                        .into(holder.image)
                    }
                })


        } catch (e: Exception) {
            e.printStackTrace()
            FirebaseCrash.report(e)
            if (BuildConfig.LOG) Log.d("-----error-----", e.message)
        }

    }
}