package com.android.pabu.view.fragment

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.view.*
import com.android.pabu.R
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.transition.Fade
import android.util.Log
import com.android.pabu.model.entity.Brand
import com.android.pabu.model.extra.Content
import com.android.pabu.model.extra.Banner
import com.android.pabu.util.*
import com.android.pabu.view.adapter.pager.SlideBannerPagerAdapter
import com.android.pabu.view.adapter.recycler.BrandRecyclerAdapter
import com.android.pabu.view.adapter.recycler.ContentRecyclerAdapter
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import dagger.android.support.AndroidSupportInjection
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import kotlinx.android.synthetic.main.fragment_content.*
import java.text.FieldPosition

class ContentFragment: BaseFragment() {

    var TAG: String = ContentFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null

    var contents: MutableList<Content> = arrayListOf()

    companion object {
        fun create(): ContentFragment = ContentFragment()
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_content, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun initialize() {
        var xContent = getResources().getStringArray(R.array.content)

        if(contents==null || contents.size < 1) {
            for (i in 0..4) {
                contents.add(Content(1, "Contentmonier " + (i + 1), "Job desc no " + (i+1), "", xContent[i].toString()))
            }
        }

        Content.ShowRecycler(content_recycler, contents, object: ContentRecyclerAdapter.ContentClickListener{
            override fun onContentClicked(holder: ContentRecyclerAdapter.ContentHolder, content: Content, position: Int) {
                val contentFragment = DetailFragment.create(Constants.FRAGMENT_CONTENT,R.layout.fragment_content_detail,position)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    contentFragment.setSharedElementEnterTransition(DetailsTransition())
                    contentFragment.setEnterTransition(Fade())
                    exitTransition = Fade()
                    contentFragment.setSharedElementReturnTransition(DetailsTransition())
                }

                activity!!.supportFragmentManager
                        .beginTransaction()
                        .addSharedElement(holder.image!!, "contentImageAnim")
                        .addSharedElement(holder.name!!, "contentTitleAnim")
                        .replace(R.id.root_container, contentFragment)
                        .addToBackStack(TAG)
                        .commit()
            }
        }, Globals)
        OverScrollDecoratorHelper.setUpOverScroll(content_recycler, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL)
    }
}