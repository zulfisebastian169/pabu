package com.android.pabu.view.fragment

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.view.*
import com.android.pabu.R
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.android.pabu.model.entity.Brand
import com.android.pabu.model.extra.Agenda
import com.android.pabu.model.extra.Banner
import com.android.pabu.util.*
import com.android.pabu.view.adapter.pager.SlideBannerPagerAdapter
import com.android.pabu.view.adapter.recycler.BrandRecyclerAdapter
import com.android.pabu.view.adapter.recycler.AgendaRecyclerAdapter
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import kotlinx.android.synthetic.main.fragment_agenda.*
import kotlinx.android.synthetic.main.fragment_agenda_detail.*
import kotlinx.android.synthetic.main.fragment_content_detail.*

class DetailFragment: BaseFragment() {

    var TAG: String = DetailFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null
    var extraFrom: String? = null
    var titleVal: String? = null
    var extraLayout: Int? = null
    var extraId: Int? = null

    var agendas: MutableList<Agenda> = arrayListOf()

    companion object {
        fun create(_from: String, _layoutId: Int, _id: Int): DetailFragment {
            val args = Bundle()
            args.putString(Constants.EXTRA_WHERE, _from)
            args.putInt(Constants.EXTRA_LAYOUT_ID, _layoutId)
            args.putInt(Constants.EXTRA_ITEM_ID, _id)

            val fragment = DetailFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        extraFrom = arguments!!.getString(Constants.EXTRA_WHERE)
        extraLayout = arguments!!.getInt(Constants.EXTRA_LAYOUT_ID, 0)
        extraId = arguments!!.getInt(Constants.EXTRA_ITEM_ID, 0)
        val rootView: View = inflater.inflate(extraLayout!!, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun initialize() {
        if(extraFrom!! == Constants.FRAGMENT_CONTENT){
            var xContent = getResources().getStringArray(R.array.content)

            Globals.picasso.load(xContent[extraId!!].toString())
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .fit().centerInside()
                    .into(content_detail_img!!, object : com.squareup.picasso.Callback {
                        override fun onSuccess() {}
                        override fun onError() {
                            Globals.picasso.load(xContent[extraId!!].toString())
                                    .fit().centerInside()
                                    .into(content_detail_img)
                        }
                    })
            backGoContent.setOnClickListener{(mContext as Activity).onBackPressed()}
        }else {
            backGo.setOnClickListener { (mContext as Activity).onBackPressed() }
        }
    }
}