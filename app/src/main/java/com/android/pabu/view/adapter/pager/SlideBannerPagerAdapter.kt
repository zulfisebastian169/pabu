package com.android.pabu.view.adapter.pager

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.NetworkPolicy
import com.android.pabu.R
import com.android.pabu.model.extra.Banner
import com.android.pabu.util.Constants
import com.android.pabu.util.Globals
import com.android.pabu.view.fragment.HomeFragment
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class SlideBannerPagerAdapter(
        val mContext: Context,
        val banners: MutableList<Banner>,
        val layoutId: Int,
        val Globals: Globals) : PagerAdapter() {
    private var layoutInflater: LayoutInflater? = null
    var TAG: String = HomeFragment::class.java.simpleName

    override fun getCount(): Int {
        return banners.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater!!.inflate(layoutId, null)
        val imageView = view.findViewById(R.id.imageView) as ImageView
        val tv_text = view.findViewById(R.id.tv_text) as TextView

        val banner = banners[position]

        view.setOnClickListener {

        }

        try {
            if(banner.photoUrl != "" && banner.photoUrl != null){
                Globals.picasso.load(banner.photoUrl)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                        .fit()
                    .into(imageView, object : com.squareup.picasso.Callback {
                        override fun onSuccess() {}
                        override fun onError() {
                            Globals.picasso.load(banner.photoUrl)
                                    .fit()
                                    .into(imageView)
                        }
                    })
            }
            tv_text.text = banner.name
        }catch (ex: Error){
            Globals.myToast(mContext, "Maaf image belum di set")
        }

        val vp = container as ViewPager
        vp.addView(view, 0)
        return view

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)

    }
}