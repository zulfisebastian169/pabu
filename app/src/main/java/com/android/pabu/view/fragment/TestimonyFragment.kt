package com.android.pabu.view.fragment

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.view.*
import com.android.pabu.R
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.android.pabu.model.entity.Brand
import com.android.pabu.model.extra.Banner
import com.android.pabu.model.extra.Testimony
import com.android.pabu.util.*
import com.android.pabu.view.adapter.pager.SlideBannerPagerAdapter
import com.android.pabu.view.adapter.recycler.BrandRecyclerAdapter
import com.android.pabu.view.adapter.recycler.TestiRecyclerAdapter
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import dagger.android.support.AndroidSupportInjection
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import kotlinx.android.synthetic.main.fragment_testimony.*

class TestimonyFragment: BaseFragment() {

    var TAG: String = TestimonyFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null

    var testis: MutableList<Testimony> = arrayListOf()

    companion object {
        fun create(): TestimonyFragment = TestimonyFragment()
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_testimony, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun initialize() {

        var xPhoto = getResources().getStringArray(R.array.photo)
        var xTesti = getResources().getStringArray(R.array.testy)

        if(testis==null || testis.size < 1) {
            for (i in 0..3) {
                testis.add(Testimony(1, "Testimonier " + (i + 1), "Job desc no " + (i+1), xPhoto[i].toString(), xTesti[i].toString()))
            }
        }
        configureVideoView()

        Testimony.ShowRecycler(testi_recycler, testis, object: TestiRecyclerAdapter.TestiClickListener{
            override fun onTestiClicked(holder: TestiRecyclerAdapter.TestiHolder, Testi: Testimony) {

            }
        }, Globals)
        OverScrollDecoratorHelper.setUpOverScroll(testi_recycler, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL)
    }

    private fun configureVideoView() {
        val path = "android.resource://" + (mContext as Activity).packageName + "/" + R.raw.test
        video_testi?.setVideoURI(Uri.parse(path))
        video_testi?.start()
        video_testi?.setOnPreparedListener(object : MediaPlayer.OnPreparedListener{
            override fun onPrepared(mp: MediaPlayer?) {
                mp?.isLooping = true
                mp?.setVolume(0f,0f)
            }
        })
    }
}