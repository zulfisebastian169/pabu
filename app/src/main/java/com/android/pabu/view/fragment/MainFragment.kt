package com.android.pabu.view.fragment

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.pabu.R
import com.android.pabu.repository.UserRepository
import com.android.pabu.viewmodel.UserViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main.*
import android.util.Log
import android.widget.RelativeLayout
import com.android.pabu.model.entity.Setting
import com.android.pabu.model.entity.User
import com.android.pabu.repository.SettingRepository
import com.android.pabu.util.Constants
import com.android.pabu.view.activity.HomeActivity
import com.android.pabu.viewmodel.SettingViewModel
import okhttp3.ResponseBody


/**
 * Created by Hamzah Tossaro on 28/05/2018.
 */

class MainFragment: BaseFragment() {

    var TAG: String = MainFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null
    var progressStatus = 0
    var finished = false

    companion object {
        fun create(): MainFragment = MainFragment()
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_main, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onPause() {
        super.onPause()
        clearUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearUp()
    }

    fun clearUp() {
        progressStatus = 0
        finished = false
    }

    fun initialize() {
        loading = (mContext as Activity).findViewById(R.id.partProgress)

        btn_login.setOnClickListener {
//            startActivity(Intent(mContext, HomeActivity::class.java))
//            (mContext as AppCompatActivity).finish()
            submit()
        }

        checkToken()
    }

    fun checkToken(){
        userViewModel?.getUserLocal(object : UserRepository.ListenerLocal {
            override fun onSelected(_user: User?) {
                if (_user != null) {
                    if ((mContext as AppCompatActivity).isFinishing==false) {
                        startActivity(Intent(mContext, HomeActivity::class.java))
                        (mContext as AppCompatActivity).finish()
                    }
                } else if (!finished) {
                    finished = true
                    (mContext as AppCompatActivity).runOnUiThread({
                        var layoutParams = logo.layoutParams as RelativeLayout.LayoutParams
                        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
//                    layoutParams.topMargin =
                        logo.layoutParams = layoutParams
                        login_container.visibility = View.VISIBLE
                    })
                }
            }
        })
    }

    fun submit(){
        try {
            if (Globals.validateEditText(scrollView, email, layoutEmail, "Email is required") == true
                    && Globals.validateEditTextEmail(scrollView, email, layoutEmail, "Please enter valid email") == true
                    && Globals.validateEditText(scrollView, password, layoutPassword, "Password is required") == true) {
                var user = User()
                user.email = email.text.toString()
                user.password = password.text.toString()
                userViewModel?.getUserRemote(user, object : UserRepository.ListenerRemote{
                    override fun onStartRemote() {
                        Constants.isLoading = true
                        (mContext as AppCompatActivity).runOnUiThread({ loading?.visibility = View.VISIBLE })
                    }
                    override fun onProcessRemote(code: Int, errBody: ResponseBody?, user: User?) {
                        Globals.hideKeyboard(mContext as AppCompatActivity)
                        if (code===400) {
//                            val gson = GsonBuilder().create()
//                            val mError= gson.fromJson(errBody!!.string(), Response::class.java)
                            Globals.myToast(mContext!!, "Sepertinya ada kendala pada internet Anda")
                        } else {
                            if(user != null){
                                settingViewModel?.getSettingLocal(Constants.SALES_ID_TEMP, object : SettingRepository.ListenerLocal {
                                    override fun onSelected(setting: Setting?) {
                                        if (setting == null) {
                                            settingViewModel?.saveSetting(Setting(Constants.SALES_ID_TEMP, user!!.id.toString(), "", ""))
                                        }
                                    }
                                })
                                userViewModel?.saveUser(user!!)
                                startActivity(Intent(mContext, HomeActivity::class.java))
                                (mContext as AppCompatActivity).finish()
                            }else{
                                Globals.myToast(mContext!!, "Username atau password salah")
                            }
                        }
                    }
                    override fun onStopRemote(type: String) {
                        Constants.isLoading = false
                        (mContext as AppCompatActivity).runOnUiThread({ loading?.visibility = View.GONE })
                    }
                })
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if(e.message!=null) Log.d("-d-"+TAG, "submit(): "+e.message.toString())
        }
    }
}