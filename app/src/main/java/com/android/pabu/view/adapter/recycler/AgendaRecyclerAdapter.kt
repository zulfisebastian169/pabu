package com.android.pabu.view.adapter.recycler

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import com.android.pabu.R
import com.google.firebase.crash.FirebaseCrash
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.model.extra.Agenda
import com.android.pabu.util.Globals



class AgendaRecyclerAdapter(
        val layout_id: Int,
        val Agendas: MutableList<Agenda>,
        val mListener: AgendaClickListener?,
        val Globals: Globals) : RecyclerView.Adapter<AgendaRecyclerAdapter.AgendaHolder>() {
    var context: Context? = null
    var selected = -1

    interface AgendaClickListener {
        fun onAgendaClicked(holder: AgendaHolder, agenda: Agenda, position: Int)
    }

    inner class AgendaHolder(_itemView: View) : RecyclerView.ViewHolder(_itemView) {
        var container: CardView? = null
        var date_container: LinearLayout? = null
        var name: TextView? = null

        init {
            container = _itemView.findViewById<View>(R.id.container) as CardView
            if (container != null) container!!.preventCornerOverlap = false

            date_container   = _itemView.findViewById(R.id.agenda_date_cont)
            name   = _itemView.findViewById(R.id.testi_desc)
        }
    }

    override fun getItemCount(): Int {
        return this.Agendas.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgendaHolder {
        context = parent.context
        val rootView = LayoutInflater.from(context).inflate(layout_id, null, false)
        val lp = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        rootView.setLayoutParams(lp)
        return AgendaHolder(rootView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: AgendaHolder, position: Int) {
        try {
            val Agenda = Agendas[position]

            holder.container!!.setOnClickListener {
                selected = holder.adapterPosition
                mListener?.onAgendaClicked(holder, Agenda, position)
            }

            holder.date_container?.transitionName = "dateContainer_" + position
            holder.name?.transitionName = "agendaName_" + position
//
//            holder.name?.text = Globals.fromHtml(Agenda.name!!)
//
//            Globals.picasso.load(Agenda.photoUrl)
//                .networkPolicy(NetworkPolicy.OFFLINE)
//                .into(holder.image, object : com.squareup.picasso.Callback {
//                    override fun onSuccess() {}
//                    override fun onError() {
//                        Globals.picasso.load(Agenda.photoUrl).into(holder.image)
//                    }
//                })

        } catch (e: Exception) {
            e.printStackTrace()
            FirebaseCrash.report(e)
            if (BuildConfig.LOG) Log.d("-----error-----", e.message)
        }

    }
}