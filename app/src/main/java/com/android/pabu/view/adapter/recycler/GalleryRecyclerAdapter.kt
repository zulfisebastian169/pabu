package com.android.pabu.view.adapter.recycler

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.android.pabu.R
import com.google.firebase.crash.FirebaseCrash
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.model.extra.Gallery
import com.android.pabu.util.Globals
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.support.v7.widget.LinearLayoutManager
import java.net.URL
import android.widget.LinearLayout




class GalleryRecyclerAdapter(
        val layout_id: Int,
        val Gallerys: MutableList<Gallery>,
        val mListener: GalleryClickListener?,
        val Globals: Globals) : RecyclerView.Adapter<GalleryRecyclerAdapter.GalleryHolder>() {
    var context: Context? = null
    var selected = -1

    interface GalleryClickListener {
        fun onGalleryClicked(holder: GalleryHolder, Gallery: Gallery)
    }

    inner class GalleryHolder(_itemView: View) : RecyclerView.ViewHolder(_itemView) {
        var container: CardView? = null
        var image: ImageView? = null
        var name: TextView? = null

        init {
            container = _itemView.findViewById<View>(R.id.container) as CardView
            if (container != null) container!!.preventCornerOverlap = false

            image   = _itemView.findViewById(R.id.gallery_img)
        }
    }

    override fun getItemCount(): Int {
        return this.Gallerys.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryHolder {
        context = parent.context
        val layoutView = LayoutInflater.from(context).inflate(layout_id, null)
        val rcv = GalleryHolder(layoutView)
        return rcv
    }

    override fun onBindViewHolder(holder: GalleryHolder, position: Int) {
        try {
            val Gallery = Gallerys[position]

            holder.container!!.setOnClickListener {
                selected = holder.adapterPosition
                mListener?.onGalleryClicked(holder, Gallery)
            }

            holder.name?.text = Globals.fromHtml(Gallery.name!!)

            if(Gallery.photoUrl!=null || Gallery.photoUrl != "") {
                Globals.picasso.load(Gallery.photoUrl)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.error_photo)
                        .into(holder.image, object : com.squareup.picasso.Callback {
                            override fun onSuccess() {}
                            override fun onError() {
                                Globals.picasso
                                        .load(Gallery.photoUrl)
                                        .fit()
                                        .centerCrop()
                                        .placeholder(R.drawable.error_photo)
                                        .into(holder.image)
                            }
                        })
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}