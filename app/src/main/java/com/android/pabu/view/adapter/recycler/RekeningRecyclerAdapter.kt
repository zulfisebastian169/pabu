package com.android.pabu.view.adapter.recycler

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.android.pabu.R
import com.google.firebase.crash.FirebaseCrash
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.android.pabu.BuildConfig
import com.android.pabu.model.extra.Rekening
import com.android.pabu.util.Globals
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.support.v7.widget.LinearLayoutManager
import java.net.URL
import android.widget.LinearLayout
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation


class RekeningRecyclerAdapter(
        val layout_id: Int,
        val Rekenings: MutableList<Rekening>,
        val mListener: RekeningClickListener?,
        val Globals: Globals) : RecyclerView.Adapter<RekeningRecyclerAdapter.RekeningHolder>() {
    var context: Context? = null
    var selected = -1

    interface RekeningClickListener {
        fun onRekeningClicked(holder: RekeningHolder, Rekening: Rekening)
    }

    inner class RekeningHolder(_itemView: View) : RecyclerView.ViewHolder(_itemView) {
        var container: CardView? = null
        var image: ImageView? = null
        var name: TextView? = null

        init {
            container = _itemView.findViewById<View>(R.id.container) as CardView
            if (container != null) container!!.preventCornerOverlap = false

            image   = _itemView.findViewById(R.id.rek_img)
            name   = _itemView.findViewById(R.id.rek_text)
        }
    }

    override fun getItemCount(): Int {
        return this.Rekenings.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RekeningHolder {
        context = parent.context
        val layoutView = LayoutInflater.from(context).inflate(layout_id, null)
        val rcv = RekeningHolder(layoutView)
        return rcv
    }

    override fun onBindViewHolder(holder: RekeningHolder, position: Int) {
        try {
            val Rekening = Rekenings[position]

            holder.container!!.setOnClickListener {
                selected = holder.adapterPosition
                mListener?.onRekeningClicked(holder, Rekening)
            }

            holder.name?.text = Globals.fromHtml(Rekening.rekening!!)

            if(Rekening.photoUrl!=null || Rekening.photoUrl != "") {
                Globals.picasso.load(Rekening.photoUrl)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .transform(RoundedCornersTransformation(20,0))
                        .fit()
                        .centerInside()
                        .error(R.drawable.error_photo)
                        .into(holder.image, object : com.squareup.picasso.Callback {
                            override fun onSuccess() {}
                            override fun onError() {
                                Globals.picasso
                                        .load(Rekening.photoUrl)
                                        .transform(RoundedCornersTransformation(20,0))
                                        .fit()
                                        .centerInside()
                                        .error(R.drawable.error_photo)
                                        .into(holder.image)
                            }
                        })
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}