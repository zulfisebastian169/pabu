package com.android.pabu.view.activity

import android.content.Context
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.android.pabu.R
import com.android.pabu.util.Globals
import com.android.pabu.util.Constants
import com.joanzapata.iconify.Iconify
import com.joanzapata.iconify.fonts.IoniconsModule
import com.android.pabu.view.fragment.*
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {
    var TAG: String = MainFragment::class.java.simpleName
    var mContext: Context = this
    var extraFragment: String? = null
    var extraWhere: String? = null
    var extraItemId: Int? = 0
    var mBackPressed: Long = 0

    @Inject lateinit var Globals: Globals
    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Iconify.with(IoniconsModule())
        Globals.createFolder(this)

        extraFragment = intent.getStringExtra(Constants.EXTRA_FRAGMENT)
        extraWhere = intent.getStringExtra(Constants.EXTRA_WHERE)
        extraItemId = intent.getIntExtra(Constants.EXTRA_ITEM_ID, 0)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.root_container, MainFragment.create()).commit()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment>? {
        return dispatchingAndroidInjector
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        return if (Constants.isLoading == false) super.dispatchTouchEvent(ev) else true
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            if (extraFragment != null && extraFragment != "" && extraFragment != Constants.FRAGMENT_HOME) {
                super.onBackPressed()
            } else {
                if (mBackPressed + 2000 > System.currentTimeMillis()) {
                    super.onBackPressed()
                    return
                } else Globals.myToast(this, "Tap again to close apps")
                mBackPressed = System.currentTimeMillis()
            }
        }
    }
}
