package com.android.pabu.view.fragment

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.view.*
import com.android.pabu.R
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.transition.Fade
import android.util.Log
import com.android.pabu.model.entity.Brand
import com.android.pabu.model.extra.Agenda
import com.android.pabu.model.extra.Banner
import com.android.pabu.util.*
import com.android.pabu.view.adapter.pager.SlideBannerPagerAdapter
import com.android.pabu.view.adapter.recycler.BrandRecyclerAdapter
import com.android.pabu.view.adapter.recycler.AgendaRecyclerAdapter
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import dagger.android.support.AndroidSupportInjection
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import kotlinx.android.synthetic.main.fragment_agenda.*
import java.text.FieldPosition

class AgendaFragment: BaseFragment() {

    var TAG: String = AgendaFragment::class.java.simpleName
    var mContext: Context? = null
    var loading: View? = null
    var userViewModel: UserViewModel? = null
    var settingViewModel: SettingViewModel? = null

    var agendas: MutableList<Agenda> = arrayListOf()

    companion object {
        fun create(): AgendaFragment = AgendaFragment()
    }

    override fun myCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_agenda, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)

        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        settingViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override fun onAttach(_context: Context?) {
        mContext = _context
        super.onAttach(_context)
    }

    override fun onResume() {
        super.onResume()
        initialize()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun initialize() {

        var xPhoto = getResources().getStringArray(R.array.photo)
        var xAgenda = getResources().getStringArray(R.array.testy)

        if(agendas==null || agendas.size < 1) {
            for (i in 0..3) {
                agendas.add(Agenda(1, "Agendamonier " + (i + 1), "Job desc no " + (i+1), xPhoto[i].toString(), xAgenda[i].toString()))
            }
        }

        Agenda.ShowRecycler(agenda_recycler, agendas, object: AgendaRecyclerAdapter.AgendaClickListener{
            override fun onAgendaClicked(holder: AgendaRecyclerAdapter.AgendaHolder, agenda: Agenda, position: Int) {
                val qrFragment = DetailFragment.create(Constants.FRAGMENT_AGENDA,R.layout.fragment_agenda_detail, position)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    qrFragment.setSharedElementEnterTransition(DetailsTransition())
                    qrFragment.setEnterTransition(Fade())
                    exitTransition = Fade()
                    qrFragment.setSharedElementReturnTransition(DetailsTransition())
                }

                activity!!.supportFragmentManager
                        .beginTransaction()
                        .addSharedElement(holder.date_container!!, "DateContainerAnim")
                        .addSharedElement(holder.name!!, "titleDetailAnim")
                        .replace(R.id.root_container, qrFragment)
                        .addToBackStack(TAG)
                        .commit()
            }
        }, Globals)
        OverScrollDecoratorHelper.setUpOverScroll(agenda_recycler, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL)
    }
}