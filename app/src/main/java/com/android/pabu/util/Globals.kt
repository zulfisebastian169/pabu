package com.android.pabu.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.CountDownTimer
import android.os.Handler
import android.provider.OpenableColumns
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.TextInputLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.telephony.SmsManager
import android.telephony.TelephonyManager
import android.text.*
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*

import com.github.thunder413.datetimeutils.DateTimeUnits
import com.github.thunder413.datetimeutils.DateTimeUtils

import com.android.pabu.R
import com.android.pabu.di.MyLifeCycleOwner
import com.android.pabu.viewmodel.SettingViewModel
import com.android.pabu.viewmodel.UserViewModel

import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject
import android.content.SharedPreferences;
import android.graphics.*
import android.support.annotation.IdRes
import android.support.design.internal.BottomNavigationItemView
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.regex.Pattern


/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

class Globals @Inject
constructor(
        val myLifeCycleOwner: MyLifeCycleOwner,
        val settingViewModel: SettingViewModel,
        val userViewModel: UserViewModel,
        val picasso: Picasso){

    val TAG = Globals::class.java.simpleName

    interface globalListener{
        fun onLoad(view: View?){}
        fun onLoad(view: View?, alertDialog: AlertDialog?){}
    }

    fun myToast(context: Context, text: String) {
        val t = Toast.makeText(context, text, Toast.LENGTH_SHORT)
        object : CountDownTimer(Math.max(3000 - 2000, 1000).toLong(), 1000) {
            override fun onFinish() {
                t.show()
            }

            override fun onTick(millisUntilFinished: Long) {
                t.show()
            }
        }.start()
    }

    fun myToast(context: Context, text: String, time: Int) {
        val t = Toast.makeText(context, text, Toast.LENGTH_SHORT)
        object : CountDownTimer(Math.max(time - 2000, 1000).toLong(), 1000) {
            override fun onFinish() {
                t.show()
            }

            override fun onTick(millisUntilFinished: Long) {
                t.show()
            }
        }.start()
    }

    fun getImage(context: Context, ImageName: String): Drawable? {
        return ContextCompat.getDrawable(context,context.resources.getIdentifier(ImageName, "drawable", context.packageName))
    }

//    fun showInfoDialog(context: Context, info: String) {
//        val dialogBuilder = AlertDialog.Builder(context)
//        val inflater = (context as Activity).layoutInflater
//        val dialogView = inflater.inflate(R.layout.dialog_info, null)
//        val title = dialogView.findViewById<TextView>(R.id.info)
//        if(info != "") title.text = info
//
//        dialogBuilder.setView(dialogView)
//        dialogBuilder.setCancelable(false)
//        dialogBuilder.setOnKeyListener(DialogInterface.OnKeyListener { dialog, keyCode, event ->
//            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP && !event.isCanceled) {
//                dialog.cancel()
//                context.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//                return@OnKeyListener true
//            }
//            false
//        })
//        val alertDialog = dialogBuilder.createBid()
//        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        alertDialog.show()
//
//        val submit = dialogView.findViewById<AppCompatButton>(R.id.submit)
//        submit.setOnClickListener { alertDialog.dismiss() }
//    }

    fun setSpinnerVal(a: Activity, spinner: Spinner?, `val`: String) {
        if (!`val`.isEmpty()) {
            val items = arrayOf(`val`)
            val adapter = ArrayAdapter(a, android.R.layout.simple_spinner_item, items)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            if (spinner != null) spinner.adapter = adapter
        }
    }

    var folderApp: File? = null
    var folderTemp: File? = null
    var folderThumbnails: File? = null
    fun createFolder(a: Context) {
        folderApp = a.getExternalFilesDir(a.getString(R.string.app_name))
        folderApp!!.mkdirs()

        folderTemp = File(folderApp, "Temp")
        folderTemp?.mkdirs()

        folderThumbnails = File(folderApp, "Thumbnails")
        folderThumbnails?.mkdirs()
    }

    fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory)
            for (child in fileOrDirectory.listFiles())
                deleteRecursive(child)
        fileOrDirectory.delete()
    }

    fun generateName(): String {
        val d = Date()
        val uuid = UUID.randomUUID().toString().replace("-".toRegex(), "")
        val s = DateFormat.format("yy-MM-dd_hh-mm-ss", d.time)
        return s.toString() + "_" + uuid
    }

    //Form Validation Start
    fun validateEditTextEmail(sv: ScrollView, editText: EditText, textInputLayout: TextInputLayout, error: String): Boolean {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(editText.text.toString().trim { it <= ' ' }).matches()) {
            textInputLayout.error = error
            focusOnView(sv, editText)
            return false
        } else textInputLayout.error = null
        return true
    }

    fun validateEditText(sv: ScrollView, editText: EditText, textInputLayout: TextInputLayout?, error: String): Boolean {
        if (editText.text.toString().trim { it <= ' ' }.isEmpty()) {
            textInputLayout?.error = error
            focusOnView(sv, editText)
            return false
        } else textInputLayout?.error = null
        return true
    }

    fun validateSpinner(sv: ScrollView, spinner: Spinner, error: String): Boolean {
        val selectedView = spinner.selectedView
        if (spinner.adapter.count == spinner.selectedItemPosition && selectedView is TextView) {
            selectedView.error = error
            focusOnView(sv, spinner)
            return false
        }
        return true
    }
    //Form Validation End

    //Form Disable Start
    fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isEnabled = false
        editText.isCursorVisible = false
        editText.keyListener = null
        editText.setBackgroundColor(Color.TRANSPARENT)
    }

    fun disableAutoCompleteTextView(autoCompleteTextView: AutoCompleteTextView) {
        autoCompleteTextView.isFocusable = false
        autoCompleteTextView.isEnabled = false
        autoCompleteTextView.isCursorVisible = false
        autoCompleteTextView.keyListener = null
        autoCompleteTextView.setBackgroundColor(Color.TRANSPARENT)
    }

    fun disableSpinner(spinner: Spinner) {
        spinner.isFocusable = false
        spinner.isEnabled = false
        spinner.setBackgroundColor(Color.TRANSPARENT)
    }
    //Form Disable End

    //File Utility Start
    private val EOF = -1
    private val DEFAULT_BUFFER_SIZE = 1024 * 4
    @Throws(IOException::class)
    fun from(context: Context, uri: Uri): File {
        val inputStream = context.contentResolver.openInputStream(uri)
        val fileName = getFileName(context, uri)
        val splitName = splitFileName(fileName)
        var tempFile = File.createTempFile(splitName[0], splitName[1])
        tempFile = rename(tempFile, fileName)
        tempFile.deleteOnExit()
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(tempFile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            Log.d("-----error-----", e.message)
        }

        if (inputStream != null) {
            copy(inputStream, out)
            inputStream.close()
        }

        out?.close()
        return tempFile
    }

    private fun splitFileName(fileName: String): Array<String> {
        var name = fileName
        var extension = ""
        val i = fileName.lastIndexOf(".")
        if (i != -1) {
            name = fileName.substring(0, i)
            extension = fileName.substring(i)
        }

        return arrayOf(name, extension)
    }

    private fun getFileName(context: Context, uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("-----error-----", e.message)
            } finally {
                cursor?.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf(File.separator)
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    private fun rename(file: File, newName: String): File {
        val newFile = File(file.parent, newName)
        if (newFile != file) {
            if (newFile.exists() && newFile.delete()) {
                Log.d("FileUtil", "Delete old $newName file")
            }
            if (file.renameTo(newFile)) {
                Log.d("FileUtil", "Rename file to $newName")
            }
        }
        return newFile
    }

    @Throws(IOException::class)
    private fun copy(input: InputStream, output: OutputStream?): Long {
        var count: Long = 0
        var n: Int
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        do {
            n = input.read(buffer)
            if (n != EOF) break
            output!!.write(buffer, 0, n)
            count += n.toLong()
        } while (true)
        return count
    }

    @Throws(IOException::class)
    fun copyFile(src: File, dst: File) {
        val `in` = FileInputStream(src)
        try {
            val out = FileOutputStream(dst)
            try {
                // Transfer bytes from in to out
                val buf = ByteArray(1024)
                var len: Int
                do {
                    len = `in`.read(buf)
                    if(len>0) break
                    out.write(buf, 0, len)
                } while (true)
            } finally {
                out.close()
            }
        } finally {
            `in`.close()
        }
    }
    //File Utility End

    fun focusOnView(sv: ScrollView, v: View) {
        sv.post { sv.scrollTo(0, v.bottom) }
    }

    fun getDateTime(cal: Calendar, format: String, withStrip: Boolean): String {
        var month = (cal.get(Calendar.MONTH) + 1).toString()
        month = if (month.length == 1) "0$month" else month

        var day = cal.get(Calendar.DAY_OF_MONTH).toString()
        day = if (day.length == 1) "0$day" else day

        var hour = cal.get(Calendar.HOUR_OF_DAY).toString()
        hour = if (hour.length == 1) "0$hour" else hour

        var minute = cal.get(Calendar.MINUTE).toString()
        minute = if (minute.length == 1) "0$minute" else minute

        var second = cal.get(Calendar.SECOND).toString()
        second = if (second.length == 1) "0$second" else second

        var result = ""
        if (format == "datetime") {
            if (withStrip)
                result = cal.get(Calendar.YEAR).toString() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
            else
                result = cal.get(Calendar.YEAR).toString() + month + day + hour + minute + second
        } else if (format == "date") {
            if (withStrip)
                result = cal.get(Calendar.YEAR).toString() + "-" + month + "-" + day
            else
                result = cal.get(Calendar.YEAR).toString() + month + day
        } else if (format == "time") {
            if (withStrip)
                result = "$hour:$minute:$second"
            else
                result = hour + minute + second
        }
        return result
    }

    fun isEmailValid(email: String):Boolean {
        var expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        var pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        var matcher = pattern.matcher(email);
        return matcher.matches();
    }

    fun getRemainTime(d: Date): String {
        val date = Date()
        val sec = DateTimeUtils.getDateDiff(d, date, DateTimeUnits.SECONDS)
        val min = DateTimeUtils.getDateDiff(d, date, DateTimeUnits.MINUTES)
        val hour = DateTimeUtils.getDateDiff(d, date, DateTimeUnits.HOURS)
        val day = DateTimeUtils.getDateDiff(d, date, DateTimeUnits.DAYS)

        var remain = ""
        if (day > 0) remain = day.toString() + " hari, "
        if (hour > 0) remain = "$remain$hour jam, "
        if (min > 0) remain = "$remain$min menit "
        if (hour == 0 && sec > 0) remain = remain + (sec - min * 60) + " detik "
        return remain
    }

    fun getDiffTime(d: Date): String {
        val date = Date()
        // Get difference in milliseconds
        val ms = DateTimeUtils.getDateDiff(date, d, DateTimeUnits.MILLISECONDS)
        // Get difference in seconds
        val sec = DateTimeUtils.getDateDiff(date, d, DateTimeUnits.SECONDS)
        // Get difference in minutes
        val min = DateTimeUtils.getDateDiff(date, d, DateTimeUnits.MINUTES)
        // Get difference in hours
        val hour = DateTimeUtils.getDateDiff(date, d, DateTimeUnits.HOURS)
        // Get difference in days
        val day = DateTimeUtils.getDateDiff(date, d, DateTimeUnits.DAYS)

        var hasil = ""
        if (day > 0) {
            hasil = day.toString() + " days ago"
        } else if (hour > 0) {
            hasil = hour.toString() + " hours ago"
        } else if (min > 0) {
            hasil = min.toString() + " min ago"
        } else if (sec > 0) {
            hasil = "Ago"
        }
        return hasil
    }

    fun randomString(length: Int): String {
        val AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        val rnd = SecureRandom()

        val sb = StringBuilder(length)
        for (i in 0 until length) sb.append(AB[rnd.nextInt(AB.length)])
        return sb.toString()
    }

    fun md5(s: String): String {
        val MD5 = "MD5"
        try {
            // Create MD5 Hash
            val digest = java.security.MessageDigest
                    .getInstance(MD5)
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2)
                    h = "0$h"
                hexString.append(h)
            }
            return hexString.toString()

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            Log.d("-----error-----", e.message)
        }

        return ""
    }

    fun replaceFragment(context: Context, container: Int, fragment: Fragment?, tag: String, back: Boolean) {
        replaceFragment(context, container, fragment, tag, back, false)
    }
    fun replaceFragment(context: Context, container: Int, fragment: Fragment?, tag: String, back: Boolean, forceNext: Boolean) {
        val fragmentManager = (context as AppCompatActivity).supportFragmentManager
        val existingFragment = fragmentManager.findFragmentById(R.id.root_container)
        if (fragment != null && (fragment.javaClass!=existingFragment?.javaClass || forceNext==true)) {
            try {
                val transaction = fragmentManager.beginTransaction()
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                transaction.replace(container, fragment)
                if (back!!) transaction.addToBackStack(tag)
                transaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("-----error-----", e.message)
            }
        }
    }

    var smsNumber: String? = null
    var smsMessage: String? = null
    fun sendSMS(context: Context, phoneNumber: String, message: String) {
        smsNumber = phoneNumber
        smsMessage = message
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context as AppCompatActivity, arrayOf(Manifest.permission.SEND_SMS), Constants.REQUEST_SMS)
        } else {
            sendSMS(context)
        }
    }

    fun sendSMS(context: Context) {
        val sms = SmsManager.getDefault()
        sms.sendTextMessage(smsNumber, null, smsMessage, null, null)
        myToast(context, "Regisrasi kamu sedang di proses")
    }

    fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    fun hideKeyboard(activity: Activity) {
        var imm: InputMethodManager = (activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        var view: View? = activity.getCurrentFocus()
        if (view == null) view = View(activity);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    fun runRecyclerAnimation(recyclerView: RecyclerView, layoutAnimation: Int) {
        val anim = AnimationUtils.loadLayoutAnimation(recyclerView.context, layoutAnimation)
        recyclerView.layoutAnimation = anim
        recyclerView.adapter?.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
    }

    fun getUniqueIMEIId(context: Context): String {
        try {
            val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(context as Activity, arrayOf(Manifest.permission.READ_PHONE_STATE), 0)
                return ""
            }
            val imei: String = telephonyManager.deviceId
            if (!imei.isEmpty()) return imei
            else return "no-imei"

        } catch (e: Exception) {
            e.printStackTrace()
            if(e.message!=null) Log.d("-d-"+TAG, "getUniqueIMEIId(): "+e.message.toString())
        }
        return "error"
    }


    fun makeItTwoDigit(number: Int, from: String):String{
        if(from == "time") {
            if (number < 10) return ("0   " + number)
            else {
                var s = StringBuilder()
                for (c: Char in number.toString().toCharArray()) s.append(c).append("   ");
                return (s.toString())
            }
        }else{
            if (number < 10) return("0"+number.toString()) else return number.toString()
        }
    }

    fun removeThousand(number: String, loop: Int): String {
        var result = ""
        val length = number.length
        if (length>3) {
            result = number.substring(0, length - 3)
            //Log.d("removeThousand-result1", result)
            if(result.length > 3 && loop<3) result = removeThousand(result,loop + 1)
            else {
                if (loop!=0) {
                    var a = number.substring(1, 2).toInt()
                    //Log.d("removeThousand-a1", a.toString())

//                    if (a==0) a = number.substring(1,2).toInt()
//                    else a = number.substring(1,3).toInt()
//                    Log.d("removeThousand-a2", a.toString())

                    result = result + "." + a
                    //Log.d("removeThousand-result2", result)
                }

                if (loop==0) result = result + "rb"
                else if (loop==1) result = result + "jt"
                else if (loop==2) result = result + "m"
                else if (loop==3) result = result + "t"
                result = "Rp" + result
            }
        }
        return result
    }

    fun showPopUpMessage(context: Context, mListener: globalListener?){
        if ((context as AppCompatActivity).isFinishing==false) {
            val dialogBuilder = AlertDialog.Builder(context)
            val inflater = (context as Activity).layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_message_image, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)
            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            mListener?.onLoad(dialogView)
            mListener?.onLoad(dialogView, alertDialog)

            alertDialog?.show()
        }
    }

    fun showPopUpHighlight(context: Context, mListener: globalListener?){
        if ((context as AppCompatActivity).isFinishing==false) {
            val dialogBuilder = AlertDialog.Builder(context)
            val inflater = (context as Activity).layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_message_highlight, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)
            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            mListener?.onLoad(dialogView)
            mListener?.onLoad(dialogView, alertDialog)

            alertDialog?.show()
        }
    }

    fun showPopUpOkCancel(context: Context, mListener: globalListener?){
        if ((context as AppCompatActivity).isFinishing==false) {
            val dialogBuilder = AlertDialog.Builder(context)
            val inflater = (context as Activity).layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_button_ok_cancel, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)
            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            mListener?.onLoad(dialogView)
            mListener?.onLoad(dialogView, alertDialog)

            alertDialog?.show()
        }
    }

    fun random(from: Int, to: Int) : Int {
        val random = Random()
        return random.nextInt(to - from) + from
    }

    fun truncateAll() {
        userViewModel.truncateAll()
    }

    fun doubleToStringNoDecimal(d: Double): String {
        val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
        formatter.applyPattern("#,###")
        return formatter.format(d)
    }

    var pref: SharedPreferences? = null

    fun init(context: Context) {
        pref = context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
    }

    fun shouldShowSlider() : Boolean {
        return (pref == null) || pref!!.getBoolean(Constants.IS_FIRST_TIME_LAUNCH, true);
    }

    fun saveFirstTimeLaunch(isFirstTime: Boolean) {
        if (pref == null)
            return;

        var editor = pref!!.edit()
        if (editor != null) {
            editor.putBoolean(Constants.IS_FIRST_TIME_LAUNCH, isFirstTime);
            editor.apply();
        }
    }

    fun checkClock(context: Context): Boolean {
        val activity = (context as Activity)
        val root_container = activity.findViewById<FrameLayout>(R.id.root_container)

        if (Constants.currentMillis!! > 0) {
            if (Constants.clockHandler == null) Constants.clockHandler = Handler()
            Constants.clockHandler!!.removeCallbacksAndMessages(null)

            Constants.clockHandler!!.postDelayed(object : Runnable {
                override fun run() {
                    if(Constants.lockMillis!! < Constants.currentMillis!!) {
                        Constants.lockMillis = Constants.currentMillis!!
                        Constants.currentSecond = 0
                    } else Constants.currentSecond = Constants.currentSecond!! + 1

                    val calendar = Calendar.getInstance()
                    calendar.timeInMillis = Constants.lockMillis!!
                    calendar.add(Calendar.SECOND, Constants.currentSecond!!)

                    val format = SimpleDateFormat("HH:mm:ss")
                    root_container?.setPadding(0,dp(context,25),0,0)

                    Constants.clockHandler?.postDelayed(this, 1000)
                }
            }, 1000)
            return true
        } else {
            root_container?.setPadding(0,0,0,0)
            return false
        }
    }

    fun dp(context: Context, px: Int): Int {
        return (px * context.resources.getDisplayMetrics().density + 0.5f).toInt()
    }

    fun getLocationOnScreen(view: View): PointF {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        return PointF(location[0].toFloat(), location[1].toFloat())
    }

    fun round(value: Double, precision: Int): Double {
        val scale = Math.pow(10.0, precision.toDouble()).toInt()
        return Math.round(value * scale).toDouble() / scale
    }

//    fun showMenuBadge(context: Context, bottomNavigationView: BottomNavigationView?, @IdRes itemId: Int, value: String) {
//        val itemView: BottomNavigationItemView? = bottomNavigationView?.findViewById(itemId)
//        val badge: View? = LayoutInflater.from(context).inflate(R.layout.menu_badge, bottomNavigationView, false)
//
////        val text = badge?.findViewById<TextView>(R.id.badge_text_view)
////        text?.setText(value)
//        itemView?.addView(badge)
//    }

    fun removeMenuBadge(bottomNavigationView: BottomNavigationView?, @IdRes itemId: Int) {
        val itemView: BottomNavigationItemView? = bottomNavigationView?.findViewById(itemId)
        if (itemView?.getChildCount() == 3) itemView.removeViewAt(2)
    }
}