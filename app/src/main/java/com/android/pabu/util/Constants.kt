package com.android.pabu.util

import android.os.Handler
import com.android.vending.billing.IInAppBillingService

/**
 * Created by hamza on 4/12/2018.
 */

object Constants {
    //USEABLE
    var isLoading: Boolean = false
    var mService: IInAppBillingService? = null
    var clockHandler: Handler? = null
    var lockMillis: Long = 0
    var currentMillis: Long = 0
    var currentSecond: Int = 0
    val BANNER_DELAY: Long = 10000

    //FRAGMENT CODES
    val FRAGMENT_HOME: String = "home"
    val FRAGMENT_HISTORY: String = "history"
    val FRAGMENT_AGENDA: String = "agenda"
    val FRAGMENT_CONTENT: String = "content"

    //REQUEST CODE
    val REQUEST_SMS: Int = 101
    val CAMERA_PERMISSION: Int = 102
    val CALL_PHONE_PERMISSION: Int = 103

    //EXTRAS
    val EXTRA_FRAGMENT: String = "extra_fragment"
    val EXTRA_WHERE: String = "extra_where"
    val EXTRA_STRING: String = "extra_string"
    val EXTRA_RESULT: String = "extra_result"
    val EXTRA_LAYOUT_ID: String = "extra_layout_id"
    val EXTRA_ITEM_ID: String = "extra_item_id"

    //Splash and TOUR
    val PREF_NAME: String = "WOWBID APPS"
    val IS_FIRST_TIME_LAUNCH: String = "FIRST_TIME_LAUNCH"

    //ID TEMP
    val EVENT_ID_TEMP = "temp_event_id"
    val COIN_ID_TEMP = "temp_coin_id"
    val SALES_ID_TEMP = "temp_sales_id"
}
