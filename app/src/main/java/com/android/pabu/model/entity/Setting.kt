package com.android.pabu.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Entity
class Setting {
    @PrimaryKey
    var uid: String = ""
    var value: String? = null
    var createdAt: String? = null
    var updatedAt: String? = null

    constructor()
    constructor(_uid: String){
        this.uid = _uid
    }
    constructor(_uid: String, _value: String, _createdAt: String, _updatedAt: String) {
        this.uid = _uid
        this.value = _value
        this.createdAt = _createdAt
        this.updatedAt = _updatedAt
    }
}
