package com.android.pabu.model.extra

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.gson.annotations.SerializedName

class Product (
    @SerializedName(value = "code") var code: String = "",
    @SerializedName(value = "name") var name: String = "",
    @SerializedName(value = "coin") var coin: Int = 0,
    @SerializedName(value = "price") var price: Int = 0) {

}