package com.android.pabu.model.extra

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.android.pabu.R
import com.android.pabu.util.Globals
import com.android.pabu.view.adapter.recycler.AgendaRecyclerAdapter
import com.google.gson.annotations.SerializedName

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Entity
class Agenda {
    @PrimaryKey
    @SerializedName(value = "id") var id: Int = 0
    @SerializedName(value = "name") var name: String? = null
    @SerializedName(value = "date") var date: String? = null
    @SerializedName(value = "description") var description: String? = null
    @SerializedName(value = "photoUrl") var photoUrl: String? = null

    constructor(_id: Int,
                _name: String,
                _date: String,
                _description: String,
                _photoUrl: String){
        this.id = _id
        this.name = _name
        this.date = _date
        this.description = _description
        this.photoUrl = _photoUrl
    }

    companion object {
        var TAG: String = Agenda::class.java.simpleName
        var Agendas: MutableList<Agenda>? = null
        var adapter: AgendaRecyclerAdapter? = null

        fun ShowRecycler(recyclerView: RecyclerView, _Agendas: MutableList<Agenda>,
                         listener: AgendaRecyclerAdapter.AgendaClickListener?, globals: Globals) {
            Agendas = _Agendas
            adapter = AgendaRecyclerAdapter(R.layout.row_agenda, _Agendas, listener, globals)
            val mContext = recyclerView.context
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = adapter
        }
    }

}



