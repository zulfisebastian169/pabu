package com.android.pabu.model.extra

import com.google.gson.annotations.SerializedName
import com.android.pabu.model.entity.Event
import com.android.pabu.model.entity.Transaction

class Response (
        @SerializedName(value = "total_price") var totalPrice: Int? = 0,
        @SerializedName(value = "quantity") var qty: Int? = 0,
        @SerializedName(value = "total") var total: Int? = 0,
        @SerializedName(value = "products") var products: MutableList<Product>? = null,
        @SerializedName(value = "events") var events: MutableList<Event>? = null,
        @SerializedName(value = "payments") var payments: MutableList<Payment>? = null,
        @SerializedName(value = "transactions") var transactions: MutableList<Transaction>? = null)
{
}