package com.android.pabu.model.extra

import com.google.gson.annotations.SerializedName

class Bonus (
    @SerializedName(value = "event_name") var eventName: String = "",
    @SerializedName(value = "customer_name") var customerName: String = "",
    @SerializedName(value = "customer_email") var customerEmail: String = "",
    @SerializedName(value = "sales_name") var salesName: String = "",
    @SerializedName(value = "total_bonus") var totalBonus: Int = 0,
    @SerializedName(value = "user_number_today") var totalInstall: Int = 0
    ) {
}