package com.android.pabu.model.local.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.android.pabu.model.entity.*

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Dao
interface UserDao {
    @Insert(onConflict = REPLACE)
    fun insertUser(user: User)
    @Update
    fun updateUserLocal(user: User)
    @Query("SELECT * FROM User limit 1")
    fun getUser(): User
    @Query("DELETE FROM User")
    fun truncateUser()

    //Event
}