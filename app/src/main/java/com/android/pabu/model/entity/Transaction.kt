package com.android.pabu.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.gson.annotations.SerializedName

@Entity
class Transaction (
    @PrimaryKey
    @SerializedName(value = "id") var id: Int = 0,
    @SerializedName(value = "name") var name: String = "",
    @SerializedName(value = "order_no") var orderNo: String = "",
    @SerializedName(value = "product_name") var productName: String = "",
    @SerializedName(value = "customer_name") var customerName: String = "",
    @SerializedName(value = "customer_email") var customerEmail: String = "",
    @SerializedName(value = "customer_phone") var customerPhone: String = "",
    @SerializedName(value = "value") var value: Int = 0,
    @SerializedName(value = "sales_name") var salesName: String = "",
    @SerializedName(value = "date") var date: String = "",
    @SerializedName(value = "status_code") var statusCode: String = "",
    @SerializedName(value = "status_name") var statusName: String = ""
    ) {

}