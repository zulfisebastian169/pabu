package com.android.pabu.model.extra

import com.google.gson.annotations.SerializedName

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

class Gallery (
        @SerializedName(value = "id") var id: Int = 0,
        @SerializedName(value = "name") var name: String? = null,
        @SerializedName(value = "target") var target: String? = null,
        @SerializedName(value = "param") var param: String? = null,
        @SerializedName(value = "photoUrl") var photoUrl: String? = null
)
