package com.android.pabu.model.extra

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.android.pabu.R
import com.android.pabu.util.Globals
import com.android.pabu.view.adapter.recycler.ContentRecyclerAdapter
import com.google.gson.annotations.SerializedName

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Entity
class Content {
    @PrimaryKey
    @SerializedName(value = "id") var id: Int = 0
    @SerializedName(value = "name") var name: String? = null
    @SerializedName(value = "date") var date: String? = null
    @SerializedName(value = "description") var description: String? = null
    @SerializedName(value = "photoUrl") var photoUrl: String? = null

    constructor(_id: Int,
                _name: String,
                _date: String,
                _description: String,
                _photoUrl: String){
        this.id = _id
        this.name = _name
        this.date = _date
        this.description = _description
        this.photoUrl = _photoUrl
    }

    companion object {
        var TAG: String = Content::class.java.simpleName
        var Contents: MutableList<Content>? = null
        var adapter: ContentRecyclerAdapter? = null

        fun ShowRecycler(recyclerView: RecyclerView, _Contents: MutableList<Content>,
                         listener: ContentRecyclerAdapter.ContentClickListener?, globals: Globals) {
            Contents = _Contents
            adapter = ContentRecyclerAdapter(R.layout.row_content, _Contents, listener, globals)
            val mContext = recyclerView.context
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = adapter
        }
    }

}



