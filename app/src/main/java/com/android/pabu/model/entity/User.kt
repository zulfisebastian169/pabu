package com.android.pabu.model.entity

import android.arch.persistence.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Entity
class User {
    @PrimaryKey
    @SerializedName(value = "id") @Expose var id: Int = 0
    @SerializedName(value = "token") @Expose var token: String? = null
    @SerializedName(value = "name") @Expose var name: String? = null
    @SerializedName(value = "email") @Expose var email: String? = null
    @SerializedName(value = "password") @Expose var password: String? = null
    @SerializedName(value = "role") @Expose var role: String? = null
    @SerializedName(value = "phone") @Expose var phone: String? = null
    @SerializedName(value = "image_url") @Expose var imageUrl: String? = null

    constructor()
    constructor(_email: String, _password: String){
        this.email = _email
        this.password = _password
    }
}