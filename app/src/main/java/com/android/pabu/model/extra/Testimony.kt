package com.android.pabu.model.extra

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.android.pabu.R
import com.android.pabu.util.Globals
import com.android.pabu.view.adapter.recycler.TestiRecyclerAdapter
/**
 * Created by Hamzah Tossaro on 12/05/2018.
 */

@Entity
class Testimony {
    @PrimaryKey
    @SerializedName(value = "id") @Expose var id: Int = 0
    @SerializedName(value = "name") @Expose var name: String? = null
    @SerializedName(value = "note_1") @Expose var note_1: String? = null
    @SerializedName(value = "photoUrl") @Expose var photoUrl: String? = null
    @SerializedName(value = "description") @Expose var description: String? = null

    constructor(_id: Int,
                 _name: String,
                 _note_1: String,
                 _photoUrl: String,
                 _description: String){
        this.id = _id
        this.name = _name
        this.note_1 = _note_1
        this.photoUrl = _photoUrl
        this.description = _description
    }

    companion object {
        var TAG: String = Testimony::class.java.simpleName
        var Testis: MutableList<Testimony>? = null
        var adapter: TestiRecyclerAdapter? = null

        fun ShowRecycler(recyclerView: RecyclerView, _Testis: MutableList<Testimony>,
                         listener: TestiRecyclerAdapter.TestiClickListener?, globals: Globals) {
            Testis = _Testis
            adapter = TestiRecyclerAdapter(R.layout.row_testimony, _Testis, listener, globals)
            val mContext = recyclerView.context
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = adapter
        }
    }
}
