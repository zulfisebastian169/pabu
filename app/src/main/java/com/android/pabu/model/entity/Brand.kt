package com.android.pabu.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.android.pabu.R
import com.android.pabu.util.Globals
import com.android.pabu.view.adapter.recycler.BrandRecyclerAdapter

/**
 * Created by Hamzah Tossaro on 12/05/2018.
 */

@Entity
class Brand {
    @PrimaryKey
    @SerializedName(value = "id") @Expose var id: Int = 0
    @SerializedName(value = "name") @Expose var name: String? = null
    @SerializedName(value = "photoUrl") @Expose var photoUrl: String? = null

    constructor(_id: Int, _name: String, _photoUrl: String){
        this.id = _id
        this.name = _name
        this.photoUrl = _photoUrl
    }

    companion object {
        var TAG: String = Brand::class.java.simpleName
        var brands: MutableList<Brand>? = null
        var adapter: BrandRecyclerAdapter? = null

        fun ShowRecycler(recyclerView: RecyclerView, _brands: MutableList<Brand>,
                         listener: BrandRecyclerAdapter.BrandClickListener?, globals: Globals) {
            brands = _brands
            adapter = BrandRecyclerAdapter(R.layout.row_brand, _brands, listener, globals)
            val mContext = recyclerView.context
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = adapter
        }
    }
}
