package com.android.pabu.model.extra

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.gson.annotations.SerializedName
import com.android.pabu.util.Globals

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

class Payment {
    @SerializedName(value = "id") var id: Int = 0
    @SerializedName(value = "code") var code: String = ""
    @SerializedName(value = "name") var name: String = ""
    var selected = false

}