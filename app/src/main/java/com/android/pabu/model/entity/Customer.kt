package com.android.pabu.model.entity

import android.arch.persistence.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Entity
class Customer {
    @PrimaryKey
    @SerializedName(value = "id") @Expose var id: Int = 0
    @SerializedName(value = "fullName") @Expose var fullname: String? = null
    @SerializedName(value = "email") @Expose var email: String? = null
    @SerializedName(value = "phone") @Expose var phone: String? = null

    constructor()
}