package com.android.pabu.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

//@Entity(foreignKeys = [ForeignKey(entity=User::class, parentColumns=["id"], childColumns=["user_id"], onDelete= ForeignKey.CASCADE)])
@Entity
class Profile {
    @PrimaryKey
    @SerializedName(value = "id") @Expose var id: Int = 0
    //@SerializedName(value = "user_id") @Expose var user_id: Int = 0
    @SerializedName(value = "birthday") @Expose var birthday: String? = ""
    @SerializedName(value = "gender") @Expose var gender: String? = ""
    @SerializedName(value = "location") @Expose var location: String? = ""
    @SerializedName(value = "shareCode") @Expose var shareCode: String? = ""
    @SerializedName(value = "coin") @Expose var coin: Int? = 0
    @SerializedName(value = "wow") @Expose var wow: Int? = 0
    @SerializedName(value = "badgeLevel") @Expose var badgeLevel: Int? = 0
    @SerializedName(value = "badgePoint") @Expose var badgePoint: Int? = 0
    @SerializedName(value = "picture") @Expose var picture: String? = ""
    @SerializedName(value = "fullName") @Expose var fullName: String? = ""
    @SerializedName(value = "firstInstall") @Expose var firstInstall: String? = ""
    @SerializedName(value = "isUserGoogle") @Expose var isUserGoogle: Boolean? = false
    @SerializedName(value = "isUserFacebook") @Expose var isUserFacebook: Boolean? = false
}