package com.android.pabu.model.local.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.android.pabu.model.entity.Transaction

@Dao
interface TransactionDao {
    @Insert(onConflict = REPLACE)
    fun insertTransaction(transaction: Transaction )
    @Update
    fun updateTransactionLocal(transaction: Transaction)
    @Query("SELECT * FROM `Transaction` limit 1")
    fun getTransaction(): Transaction
    @Query("DELETE FROM `Transaction`")
    fun truncateTransaction()
}