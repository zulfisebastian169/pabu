package com.android.pabu.model.local.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.android.pabu.model.entity.Setting

/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Dao
interface SettingDao {
    @Insert(onConflict = REPLACE)
    fun insertSetting(setting: Setting?)

    @Query("SELECT * FROM Setting where uid = :uid limit 1")
    fun getSetting(uid: String): Setting

    @Query("SELECT * FROM Setting where uid like :uid")
    fun getSettings(uid: String): List<Setting>

    @Query("SELECT * FROM Setting where createdAt like :createdAt limit 1")
    fun getSettingByCreated(createdAt: String): Setting

    @Delete
    fun deleteSetting(setting: Setting?)

    @Query("DELETE FROM Setting")
    fun truncate()
}