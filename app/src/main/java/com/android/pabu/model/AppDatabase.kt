package com.android.pabu.model.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.android.pabu.model.entity.*
import com.android.pabu.model.local.dao.*
import com.android.pabu.util.DateConverter


/**
 * Created by Hamzah Tossaro on 10/21/2018.
 */

@Database(entities = [
    Event::class,
    Transaction::class,
    Setting::class,
    User::class
], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun settingDao(): SettingDao
    abstract fun userDao(): UserDao
    abstract fun eventDao(): EventDao
    abstract fun transactionDao(): TransactionDao

    companion object {
//        val MIGRATION_20_21: Migration = object : Migration(20, 21) {
//            override fun migrate(database: SupportSQLiteDatabase) {
//                database.execSQL("ALTER TABLE User " + " ADD COLUMN referralCode TEXT")
//            }
//        }
    }
}