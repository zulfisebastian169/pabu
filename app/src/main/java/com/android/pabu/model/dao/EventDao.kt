package com.android.pabu.model.local.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.android.pabu.model.entity.*

@Dao
interface EventDao {
    @Insert(onConflict = REPLACE)
    fun insertEvent(event: Event)
    @Update
    fun updateEventLocal(event: Event)
    @Query("SELECT * FROM Event limit 1")
    fun getEvent(): Event
    @Query("DELETE FROM Event")
    fun truncateEvent()
}