package com.android.pabu.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class Event (
    @PrimaryKey
    @SerializedName(value = "id") var id: Int = 0,
    @SerializedName(value = "name") var name: String = "",
    @SerializedName(value = "address") var address: String = "",
    @SerializedName(value = "start_date") var startDate: String = "",
    @SerializedName(value = "finish_date") var finishDate: String = "",
    @SerializedName(value = "imageurl") var imageurl: String = "") {

    companion object {
    }
}